﻿using System.Web;
using System.Web.Mvc;

namespace Task_Flex_Group
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
