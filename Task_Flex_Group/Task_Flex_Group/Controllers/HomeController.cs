﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using HtmlAgilityPack;
using System.Web.Mvc;
using Task_Flex_Group.Models;

namespace Task_Flex_Group.Controllers
{
    public class HomeController : Controller
    {
        private ResultsEntities db = new ResultsEntities();
        private const int SERVERERROR = 0;

        private int getResponseTime(string url)
        {
            try
            {
                WebRequest request = HttpWebRequest.Create(url);
                Stopwatch timer = Stopwatch.StartNew();
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    timer.Stop();
                    return timer.Elapsed.Milliseconds;
                }
            }

            catch (Exception)
            {
                return SERVERERROR;
            }
        }

        private HashSet<string> getSitemapUrls(string url)
        {
            HtmlWeb web = new HtmlWeb();
            HtmlDocument document = web.Load(url);
            List<string> addresses = document.DocumentNode.SelectNodes("//a[@href]").Select( a => a.Attributes["href"].Value).Where( a => !string.IsNullOrWhiteSpace(a) && (a.StartsWith("http") || a.StartsWith(@"/"))).ToList();
            HashSet<string> websites = new HashSet<string>();
            foreach (var address in addresses)
            {
                string element = (address.StartsWith(@"/")) ? url + address : address;
                websites.Add(element);
            }
            return websites;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddUrl(Pages page)
        {
            try
            {
                string url = page.Url;
                HashSet<string> websites = getSitemapUrls(url);
                List<Results> testResults = new List<Results>();
                Results result;
                foreach (var website in websites)
                {
                    int response = getResponseTime(website);
                    if (response != SERVERERROR)
                    {
                        result = new Results();
                        result.Url = website;
                        result.ResponseTime = response;
                        testResults.Add(result);
                    }
                }
                var sortedResults = testResults.OrderByDescending(r => r.ResponseTime).ToList();
                page.MaxResponseTime = sortedResults.First().ResponseTime;
                page.MinResponseTime = sortedResults.Last().ResponseTime;
                db.Pages.Add(page);
                db.Results.AddRange(sortedResults);
                db.SaveChanges();
                ViewData["sortedData"] = sortedResults;
                return PartialView("ListMeasuredUrls", sortedResults);
            }

            catch (Exception ex)
            {
                Response.Write(ex.Message);
                return View("Index");
            }
        }

        public ActionResult ListAllResults()
        {
            var sortedResults = db.Results.OrderByDescending(r => r.ResponseTime).ToList();
            return View(sortedResults);
        }

        public ActionResult ListHistory()
        {
            return View(db.Pages.ToList());
        }
    }
}
